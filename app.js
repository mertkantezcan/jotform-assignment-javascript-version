//HTTPRequest for browser
const http = require('axios');
const home = "https://eee.jotform.pro/interview/";

http.get(home)

    .then(function (response) {
    // handle success
   
        console.log(Object.entries(response.data).map(
            ( [ key, value ] ) => `key: ${key}, name: ${value.name}, value: ${value.url}`
        ).join('\n')) 
        })
        
    .catch(function (error) {
    // handle error
    console.log('error', error);
    })
    .then(function () {
    // always executed
    });

